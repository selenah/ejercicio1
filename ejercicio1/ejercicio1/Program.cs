﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<String> ejecicio1 = new Queue<string>();
            TextReader leer;
            Console.Clear();
            leer = new StreamReader("archivo.txt");
            string palabras = leer.ReadToEnd();
            leer.Close();
            char[] separador = { ' ', ',' };
            int c = 0;
            string[] partes = palabras.Split(separador);
            for (int i = 0; i < partes.Length; i++)
            {
                if (partes[i].Length > 5)
                {
                    ejecicio1.Enqueue(partes[i]);
                    c++;
                }
            }
            for (int i = 0; i < c; i++)
            {
                Console.WriteLine(ejecicio1.Dequeue());
            }
            Console.ReadLine();
            

        }
    }
}
